<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>

<!-- Blog Sidebar Widgets Column -->
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 side pull-left">
        <div class="clear"></div>
        <!-- Blog Search Well -->
        <div class="panel panel-primary">
            <div class="panel-heading">
            <h4>Pencarian</h4>
            </div>
            
            <div class="panel-body input-group">
            <form name="form-search" method="post" action="<?php echo $website_url;?>/search-result/" style="display:inline-table;">
                <input type="text" name="search" class="form-control" placeholder="Kata Kunci">
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit">
                        <span class="fa fa-search"></span>
                	</button>
                </span>
            </form>
            </div>
            <!-- /.input-group -->
        </div>

        <!-- Blog Categories Well -->
        <div class="panel panel-primary">
            <div class="panel-heading">
            <h4>Kaitkata</h4>
                </div>
            <div class="panel-body">
            <ul>
            <?php
				$tabletag = new PoTable('tag');
				$tags = $tabletag->findAllLimit(id_tag, DESC, '10');
				foreach($tags as $tag){
			?>
				<li><a href="<?=$website_url;?>/search-result/<?=$tag->tag_title;?>" title=""><?=$tag->tag_title;?></a></li>
			<?php } ?>
			</ul>
            </div>
                
            
        </div>

        <div class="widget clearfix">
			<div class="enews-tab">
				<!-- Tab Menu -->
				<ul class="nav nav-tabs" id="enewsTabs">
					<li class="active"><a href="#tab-populars" data-toggle="tab">Populer</a></li>
					<li><a href="#tab-recents" data-toggle="tab">Terkini</a></li>
				</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab-populars">
				<?php
					$tablepop = new PoTable('post');
					$pops = $tablepop->findAllLimitBy(hits, active, 'Y', DESC, '5');
					foreach($pops as $pop){
						$tablepopc = new PoTable('comment');
						$totalpop = $tablepopc->numRowByAnd(id_post, $pop->id_post, active, 'Y');
				?>
				<div class="item">
					<div class="content">
						<p><a href="<?php echo "$website_url/tulisan/$pop->seotitle"; ?>" title="<?=$pop->title;?>"><?=$pop->title;?></a></p>
						<p class="meta"><?=$pop->hits;?> views</p>
					</div>
				</div>
				<?php } ?>
				</div> <!-- End Populars -->

				<div class="tab-pane" id="tab-recents">
				<?php
					$tablerec = new PoTable('post');
					$recs = $tablerec->findAllLimitBy(id_post, active, 'Y', DESC, '5');
					foreach($recs as $rec){
						$validrec = $rec->id_category;
						$tablecatrec = new PoTable('category');
						$currentCatrec = $tablecatrec->findBy(id_category, $validrec);
						$currentCatrec = $currentCatrec->current();
				?>
				<div class="item">
					<div class="content">
						<p><a href="<?php echo "$website_url/tulisan/$rec->seotitle"; ?>" title="<?=$rec->title;?>"><?=$rec->title;?></a></p>
						<p class="meta">Di <a href="<?php echo "$website_url/category/$currentCatrec->seotitle"; ?>"><?=$currentCatrec->title;?></a> Pada <?=tgl_indo($rec->date);?></p>
					</div>
				</div>
				<?php } ?>
				</div> <!-- End Recents -->

			</div> <!-- End Tab-Content -->

			</div> <!-- End Enews-Tab --> 
		</div> <!-- End Widget -->

    </div>

<?php } ?>