<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>

<?php include_once "po-content/$folder/header.php"; ?>

<!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=$website_url;?>"><?=$website_name;?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <?php 
                    $instance = new PoController;
                    $menu = $instance->popoji_menu(2, 'class="nav navbar-nav navbar-right" id="main-menu"', '');
                 echo $menu.PHP_EOL;
               ?>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <div class="jumbotron"></div>
    <header class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1><?=$website_name;?></h1>
                        <hr class="small">
                        <span class="subheading"><?=$meta_description;?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
<?php /*
                <div class="post-preview">
                    <a href="post.html">
                        <h2 class="post-title">
                            Man must explore, and this is exploration at its greatest
                        </h2>
                        <h3 class="post-subtitle">
                            Problems look mighty small from 150 miles up
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on September 24, 2014</p>
                </div>
                <hr>
                <div class="post-preview">
                    <a href="post.html">
                        <h2 class="post-title">
                            I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.
                        </h2>
                    </a>
                    <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on September 18, 2014</p>
                </div>
                <hr>
                <div class="post-preview">
                    <a href="post.html">
                        <h2 class="post-title">
                            Science has not yet mastered prophecy
                        </h2>
                        <h3 class="post-subtitle">
                            We predict too much for the next year and yet far too little for the next ten.
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on August 24, 2014</p>
                </div>
                <hr>
                <div class="post-preview">
                    <a href="post.html">
                        <h2 class="post-title">
                            Failure is not an option
                        </h2>
                        <h3 class="post-subtitle">
                            Many say exploration is part of our destiny, but it’s actually our duty to future generations.
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="#">Start Bootstrap</a> on July 8, 2014</p>
                </div>
                <hr>
*/
	$tablecona = new PoTable('post');
	$conas = $tablecona->findAllLimitBy(id_post, active, 'Y', DESC, '10');
	foreach($conas as $cona){
		$tableuser = new PoTable('users');
		$author = $tableuser->findBy(id_user, $cona->editor);
		$writer = $author->current();
?>
				<div class="post-preview">
                    <a href="<?php echo "$website_url/tulisan/$cona->seotitle"; ?>">
                        <h2 class="post-title">
                            <?=$cona->title.PHP_EOL;?>
                        </h2>
                        <h3 class="post-subtitle">
                            <?php echo cuthighlight('post', $cona->content, '70')."&hellip;".PHP_EOL;?>
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="https://badwi.amzone.web.id/author/badwi"><?=$writer->nama_lengkap;?></a> on <?=tgl_indo($cona->date);?></p>
                </div>
                <hr>
<?php
		//echo cuthighlight('post', $cona->content, '100').PHP_EOL;
	}
?>
                
                <!-- Pager -->
                <ul class="pager">
                    <li class="next">
                        <a href="#">Older Posts &rarr;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <hr>


<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>