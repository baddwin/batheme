<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>

<?php include_once "po-content/$folder/header.php"; ?>

<!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=$website_url;?>"><?=$website_name;?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <?php 
                    $instance = new PoController;
                    $menu = $instance->popoji_menu(2, 'class="nav navbar-nav navbar-right" id="main-menu"', '');
                 echo $menu.PHP_EOL;
               ?>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Header -->
    <!-- Set your background image for this header on the line below. -->
    <div class="jumbotron"></div>
    <header class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1><?=$website_name;?></h1>
                        <hr class="small">
                        <span class="subheading"><?=$meta_description;?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

                
		
			<!-- End Breadcrumb -->
			<h1>Hubungi Kami</h1>
			<p>Berikut ini kami lampirkan informasi kontak untuk dihubungi. Mohon jangan disalahgunakan.</p>
			<p>Slamet Badwi</p>

		    <div class="row">
			
			<div class="col-sm-4">
			    <h5>Alamat</h5>
			    <p>
				    Batang, 51261. <br />

				    Indonesia
			    </p>
			</div>
			<div class="col-sm-6">
			    <h5>Kontak</h5>
			    <p>
				    Ponsel: 085-<!--abc-->742-<!--abc-->770-<!--abc-->603 <br />

				    slamet<!--..-->.badwi@<!--abc-->gmail.com
			    </p>
			</div>
		    </div>

            </div>
        </div>
    </div>

<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>