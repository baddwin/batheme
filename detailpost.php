<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>

<?php include_once "po-content/$folder/header.php"; ?>

<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=$website_url;?>"><?=$website_name;?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <?php 
                    $instance = new PoController;
                    $menu = $instance->popoji_menu(2, 'class="nav navbar-nav navbar-right" id="main-menu"', '');
                 echo $menu.PHP_EOL;
               ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<div class="container clearfix">
<?php
	$title = $val->validasi($_GET['id'],'xss');
	$detail = new PoTable();
	$currentDetail = $detail->findManualQuery($tabel = "post,users,category", $field = "", $condition = "WHERE users.id_user = post.editor AND category.id_category = post.id_category AND category.active = 'Y' AND post.active = 'Y' AND post.seotitle = '".$title."'");
	$currentDetail = $currentDetail->current();
	$idpost = $currentDetail->id_post;

	if ($currentDetail > 0){
	$tabledpost = new PoTable('post');
	$currentDpost = $tabledpost->findByPost(id_post, $idpost);
	$currentDpost = $currentDpost->current();
	
	$contentdet = html_entity_decode($currentDetail->content);
	$biodet = html_entity_decode($currentDetail->bio);

	$tabledcat = new PoTable('category');
	$currentDcat = $tabledcat->findBy(id_category, $currentDetail->id_category);
	$currentDcat = $currentDcat->current();

	$p = new Paging;
	$batas = 5;
	$posisi = $p->cariPosisi($batas);
	$tabledcom = new PoTable('comment');
	$composts = $tabledcom->findAllLimitByAnd(id_comment, id_post, active, "$idpost", "Y", "ASC", "$posisi,$batas");
	$totaldcom = $tabledcom->numRowByAnd(id_post, $idpost, active, 'Y');

	mysql_query("UPDATE post SET hits = $currentDetail->hits+1 WHERE id_post = '".$idpost."'");
?>

	<div class="row">
            
            <!-- Blog Post Content Column -->
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main pull-right">

                <div class="row">
                    <div class="col-md-1">
                    
                        <div class="clear"></div>
                        <div class="bundaran">
<?php
$table = new PoTable('post');
$data = $table->findBy(id_post, $idpost);
//echo $data->current();
foreach ($data as $isi) {
	# code...
	echo $isi->hits.PHP_EOL;
}
//var_dump($data);
?>
                        </div>
                        
                    </div>
                    
                <div class="col-md-11">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?=$currentDpost->title;?></h1>

                <!-- Date/Time and author -->
                <p><span class="fa fa-clock-o"></span> Diterbitkan pada <time><?=tgl_indo($currentDetail->date);?></time> oleh <a href="#"><?=$currentDetail->nama_lengkap;?></a></p>

                <ul class="breadcrumb">
                  <li><a href="<?=$website_url;?>">Utama</a></li>
                  <li><a href="<?php echo "$website_url/category/$currentDcat->seotitle"; ?>" title="<?=$currentDcat->title;?>"><?=$currentDcat->title;?></a></li>
                  <li class="active"><?php echo $currentDpost->title;?></li>
                </ul>

                <!-- Preview Image -->
                <figure>
					<img src="<?php echo $website_url;?>/po-content/po-upload/<?=$currentDetail->picture;?>" alt="<?=$currentDpost->title;?>" />
				</figure>
                <hr>

                <!-- Post Content -->
                <div class="content">
					<?=$contentdet;?>
					<hr>
					<div class="tags">
						
						<strong>Kategori: </strong><a href="<?php echo "$website_url/category/$currentDcat->seotitle"; ?>" title="<?php echo $currentDcat->title;?>"><?php echo $currentDcat->title;?></a>&nbsp;
						<strong>Tags:</strong>&nbsp;
						<?php
							$tabletag = new PoTable('tag');
							$tags = $tabletag->findAll(id_tag, DESC);
							$arrtags = explode(',', $currentDetail->tag);
							foreach($tags as $tag){
								$cek = (array_search($tag->tag_seo, $arrtags) != false)? '' : 'display:none;';
								echo "<a href='$website_url/search-result/$tag->tag_title' title='$tag->tag_title' style='$cek'>$tag->tag_title</a> ";
							}
						?>
						
					</div> <!-- End Tags -->
				</div> <!-- End Content -->
                <hr>

                <div class="content-heading">
					<h4>Tentang Penulis</h4>
				</div>
				<div class="row clearfix">
				<div class="col-xs-2">
					<figure>
					<?php
						$filename = "$website_url/po-content/po-upload/user-$currentDetail->id_user.jpg";
						if (file_exists("$filename")){
							echo "<img src='$website_url/po-content/po-upload/user-$currentDetail->id_user.jpg' alt='$currentDetail->nama_lengkap' class='imgauthor' style='width:90px;height:90px;'/>";
						}else{
							echo "<img src='$website_url/po-content/po-upload/user-editor.jpg' alt='$currentDetail->nama_lengkap' class='imgauthor' style='width:90px;height:90px;'/>";
						}
					?>
					</figure>
				</div>
				<div class="col-xs-10">
					<div class="content">
						<h5><a href="#" title="<?=$currentDetail->nama_lengkap;?>"><?=$currentDetail->nama_lengkap;?></a></h5>
						<p><?=$biodet;?></p>
					</div>
					</div>
				</div> <!-- End Post Author -->
				<hr>

				<div class="related-posts">
					<h4>Post Terkait</h4>
					<?php
						$tablerelated = new PoTable('post');
						$tablerelateds = $tablerelated->findRelatedPost($currentDetail->tag, $idpost, id_post, "DESC", "5");

						foreach($tablerelateds as $related){
						//var_dump($related);
					?>
					<div class="row">
					<div class="col-md-4">
					<?php
					if (empty($related)) {
						echo "Tidak ada tulisan terkait";
					}
					?>
						<a href="<?php echo "$website_url/tulisan/$related->seotitle"; ?>" title="<?=$related->title;?>" rel="bookmark">
							<figure class="figure-hover">
								<img src="<?=$website_url;?>/po-content/po-upload/<?=$related->picture;?>" alt="<?=$related->title;?>" />
								<div class="figure-hover-masked">
									<p class="icon-plus"></p>
								</div>
							</figure>
						</a>
						<p><?=$related->title;?></p>
					</div>
					</div>
					<?php } ?>
					
				</div> <!-- End Related-Posts -->

                <div class="clear"></div>
                <hr>
                <!-- Blog Comments -->
                <p class="loadingr"><img src="<?php echo $website_url?>/loadingr.gif" alt="loading..." width="50" height="50"></p>
                <!-- Comments Form -->
                <button class="berkomen btn btn-lg btn-success">Baca atau Beri Komentar</button>
                <!-- Disqus  comment system-->
                <div id="disqus_thread"></div>
            	</div>
        	</div>
    	</div>

<?php }else{ ?>
	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main pull-right">
	<div class="row-fluid">
		<h2>Halaman Tidak Ditemukan</h2>
		<hr>
		<form name="form-search" method="post" action="<?=$website_url;?>/search-result/">
			<div class="form-group">
			  <label class="control-label" for="focusedInput">Cari halaman</label>
			  <div class="input-group">
			  <input class="form-control" id="focusedInput" name="search" placeholder="Pencarian..." type="text">
			  <span class="input-group-btn"><input class="btn btn-primary" type="submit" name="submit" value="Cari" /></span>
			  </div>
			</div>
		</form>
	</div> <!-- End Row-Fluid -->
	</div> <!-- End Main -->
<?php } ?>
            
<?php include_once "po-content/$folder/sidebar.php"; ?>
            
    </div>
    <!-- /.row -->

</div>


<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>
