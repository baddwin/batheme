<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>

<?php include_once "po-content/$folder/header.php"; ?>

<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=$website_url;?>"><?=$website_name;?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <?php 
                    $instance = new PoController;
                    $menu = $instance->popoji_menu(2, 'class="nav navbar-nav navbar-right" id="main-menu"', '');
                 echo $menu.PHP_EOL;
               ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<div class="container clearfix">
<?php
if ($_GET['search'] == ""){
	$postkata = $_POST['search'];
	header('location:'.$website_url.'/search-result/'.$postkata);
}else{
	$kata = $val->validasi($_GET['search'],'xss');
	$p = new Paging;
	$batas = 5;
	$posisi = $p->cariPosisi($batas);
	$tablesearch = new PoTable('post');
	$searchposts = $tablesearch->findSearchPost($kata, "$posisi,$batas");
	$numsearchposts = $tablesearch->numRowSearchPost($kata);

	if ($numsearchposts > 0){
?>

<div class="row">
            
            <!-- Blog Post Content Column -->
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main pull-right">

            <h2>Hasil pencarian dari kata &quot;<em><?php echo $kata;?></em>&quot;</h2>
            <hr>
			<form name="form-search" method="post" action="<?=$website_url;?>/search-result/">
				<div class="input-group">
				<input class="form-control" type="text" name="search" value="<?=$kata;?>" />
				<span class="input-group-btn"><input class="btn btn-primary" type="submit" name="submit" value="Cari" /></span>
				</div>
			</form>
			<div class="clear"></div>
			<p class="search-info">Menampilkan <em><?php echo ($numsearchposts > 10) ? "1-0 dari ".$numsearchposts : $numsearchposts;?></em> hasil pencarian</p>
			<hr>

			<?php
					foreach($searchposts as $searchpost){
						$tabledscom = new PoTable('comment');
						$totaldscom = $tabledscom->numRowByAnd(id_post, $searchpost->id_post, active, 'Y');
						$tablecatds = new PoTable('category');
						$currentCatds = $tablecatds->findBy(id_category, $searchpost->id_category);
						$currentCatds = $currentCatds->current();
						$tableuser = new PoTable('users');
						$currentUser = $tableuser->findBy(id_user, $searchpost->editor);
						$currentUser = $currentUser->current();
				?>
					<div class="post clearfix">
						<figure>
							<img src="<?=$website_url;?>/po-content/po-upload/<?=$searchpost->picture;?>" alt="<?=$searchpost->title;?>" />
							<div class="cat-name">
								<span class="base"><?=$currentCatds->title;?></span>
							</div>
						</figure>
						<div class="content">
							<h2><a href="<?php echo "$website_url/detailpost/$searchpost->seotitle"; ?>" title="<?=$searchpost->title;?>"><?=$searchpost->title;?></a></h2>
							<p><?=cuthighlight('post', $searchpost->content, '200');?>...</p>
						</div>
						<div class="meta">
							<span class="pull-left"><?=$currentCatds->title;?> - Oleh <?=$currentUser->nama_lengkap;?> - <?=tgl_indo($searchpost->date); ?></span>
							<span class="pull-right"><a href="<?php echo "$website_url/detailpost/$searchpost->seotitle"; ?>">Baca Selengkapnya...</a></span>
						</div>
					</div>
					<hr>
				<?php
					}
				?>

			<nav class="pager">
				<ul>
					<?php
						$getpage = $val->validasi($_GET['page'],'sql');
						$jmldata = $tablesearch->numRowSearchPost($kata);
						$jmlhalaman = $p->jumlahHalaman($jmldata, $batas);
						$linkHalaman = $p->navHalaman($getpage, $jmlhalaman, $website_url, "search-result", $kata);
						echo "$linkHalaman";
					?>
				</ul>
			</nav>

	</div>

<?php }else{ ?>
	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main pull-right">
	
		<h2>Halaman Tidak Ditemukan</h2>
		<hr>
		<form name="form-search" method="post" action="<?=$website_url;?>/search-result/">
			<div class="form-group">
			  <label class="control-label" for="focusedInput">Cari halaman</label>
			  <div class="input-group">
			  <input class="form-control" id="focusedInput" name="search" placeholder="Pencarian..." type="text">
			  <span class="input-group-btn"><input class="btn btn-primary" type="submit" name="submit" value="Cari" /></span>
			  </div>
			</div>
		</form>
	
	</div> <!-- End Main -->
<?php }} ?>
            
<?php include_once "po-content/$folder/sidebar.php"; ?>

</div>

<?php include_once "po-content/$folder/footer.php"; ?>
<?php } ?>