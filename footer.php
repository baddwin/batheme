<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
	<!-- Footer Widgets -->
	<hr>
<?php if ($mod == "contact") : ?>
    <section class="footer-widgets">
	
    </section>
<?php endif; ?>

<!-- Footer -->
    <footer class="site-footer">
    <div class="container">
	<div class="row">
	    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
		<ul class="list-inline text-center">
		    <li>
			<a href="https://twitter.com/baddwin">
			    <span class="fa-stack fa-lg">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
			    </span>
			</a>
		    </li>
		    <li>
			<a href="https://facebook.com/slamet.badwi">
			    <span class="fa-stack fa-lg">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
			    </span>
			</a>
		    </li>
		    <li>
			<a href="https://plus.google.com/+SlametBadwi">
			    <span class="fa-stack fa-lg">
				<i class="fa fa-circle fa-stack-2x"></i>
				<i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
			    </span>
			</a>
		    </li>
		</ul>
		<?php
		$tanggal = new DateTime("today", new DateTimeZone('Asia/Jakarta'));
		$tahun = $tanggal->format("Y");
		?>
		<p class="copyright text-muted">&copy; <?php echo $website_name;?> 2014-<?php echo $tahun;?></p>
	    </div>
	</div>
    </div>
    </footer>

    <!-- JavaScript -->
	<!--<script src="<?=$website_url;?>/po-content/<?=$folder;?>/js/jquery.js"></script>-->
	<!--<script src="<?=$website_url;?>/po-content/<?=$folder;?>/js/bootstrap.min.js"></script>-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

<?php if($mod == "home" || $mod=="contact") : ?>
	<script src="<?=$website_url;?>/po-content/<?=$folder;?>/js/clean-blog.js"></script>
	<script type="text/javascript">
		var jumboHeight = $('.intro-header').outerHeight();
		function parallax(){
		    var scrolled = $(window).scrollTop();
		    $('.jumbotron').css('height', (jumboHeight-scrolled) + 'px');
		}

		$(window).on("scroll",function(e){
		    parallax();
		});
	</script>
<?php elseif ($mod == "detailpost") : ?>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('.berkomen').on('click', function(){
		          var disqus_shortname = 'amzone';
		 
		          // ajax request to load the disqus javascript
		          $.ajax({
		                  type: "GET",
		                  url: "https://" + disqus_shortname + ".disqus.com/embed.js",
		                  dataType: "script",
		                  cache: true,
		                  beforeSend: function(){
		                  	$('.loadingr').show();
		                  },
		                  complete: function(){
		                  	$('.loadingr').fadeOut();
		                  }
		          });

		          $(this).fadeOut();
		    });
		});
	</script>
<?php endif; ?>

</body>
</html>
<?php } ?>