<?php if ($mod==""){
	header('location:../../404.php');
}else{
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="id"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="id"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="id"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="id"> <!--<![endif]-->

<head>
	<!-- Your Basic Site Informations -->
	<title><?php include "title.php"; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
    <meta name="robots" content="index, follow">
    <meta name="description" content="<?php include "meta-desc.php"; ?>">
    <meta name="keywords" content="<?php include "meta-key.php"; ?>">
    <meta http-equiv="Copyright" content="popojicms">
    <meta name="author" content="Slamet Badwi">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name='language' content='id'/>
    <meta name='geo.country' content='id'/>
    <meta name='geo.placename' content='Indonesia'/>
    <meta name="revisit-after" content="7">
    <meta name="webcrawlers" content="all">
    <meta name="rating" content="general">
    <meta name="spiders" content="all">
    <!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width">

    <!-- Stylesheets -->
    <!-- Bootstrap Core CSS -->
    <!--<link href="<?=$website_url;?>/po-content/<?=$folder;?>/css/bootstrap.min.css" rel="stylesheet">-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
<?php if ($mod== "home" || $mod == "contact") : ?>
    <link href='//fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom CSS -->
    <link href="<?=$website_url;?>/po-content/<?=$folder;?>/css/clean-blog.min.css" rel="stylesheet">

    <style type="text/css">
        .intro-header {
          height: 400px;
          /*color: white;
          text-shadow: #444 0 1px 1px;
          background:transparent;*/
        }
        .jumbotron {
          background: url('<?=$website_url;?>/po-content/<?=$folder;?>/img/home-bg.jpg') no-repeat center center;
          position: fixed;
          width: 100%;
          height: 400px; /*same height as jumbotron */
          top:0;
          left:0;
          z-index: -1;
          padding: 0;
        }
    </style>

<?php else : ?>

    <style type="text/css">
    /*!
    * Start Bootstrap - Blog Post HTML Template (http://startbootstrap.com)
    * Code licensed under the Apache License v2.0.
    * For details, see http://www.apache.org/licenses/LICENSE-2.0.
    */

    body {
	padding-top: 70px; /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }

    footer {
	padding: 50px 0 65px;
    }
    footer .list-inline {
    margin: 0;
    padding: 0;
    }
    footer .copyright {
    font-size: 14px;
    text-align: center;
    margin-bottom: 0;
    margin-top: 30px;
    }
    /* end of code */
    
    .clear {height: 25px;}
    .bundaran 
    {
        border-radius: 50%;
        background-color: #428BCA;
        height: 30px;
        text-align: center;
        width: 30px;
        margin-left: auto;
        margin-right: auto;
        padding: 4px;
        color: #fff;
    }
    .loadingr {
      text-align: center;
      display: none;
    }
    .item {
      padding: 5px 5px;
      border-bottom: 1px dashed #F1F1F1;
    }
    .meta {
      font-size: 12px;
    }

    img {
      max-width: 100%;
    }
    </style>

<?php endif; ?>
    <!-- Favicons -->
	<link rel="shortcut icon" href="<?=$website_url;?>/<?=$favicon;?>">

	<script>
	var RecaptchaOptions = {
		theme : 'clean'
	};
	</script>

</head>

<body>

<?php } ?>